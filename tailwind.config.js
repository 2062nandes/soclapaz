module.exports = {
  theme: {
    maxWidth: {
      '8xl': '80rem'
    },
    opacity: {
      '0': '0',
      '25': '.25',
      '50': '.5',
      '75': '.75',
      '10': '.1',
      '20': '.2',
      '30': '.3',
      '40': '.4',
      '50': '.5',
      '60': '.6',
      '70': '.7',
      '80': '.8',
      '90': '.9',
      '100': '1',
    },
    extend: {
      spacing: {
        '80': '20rem',
        '108': '27rem',
      },
      borderWidth: {
        '14': '14px',
      }
    },
    container: {
      padding: '0'
    },
    colors: {
      background: {
        primary: 'var(--bg-background-primary)',
        secondary: 'var(--bg-background-secondary)',
        tertiary: 'var(--bg-background-tertiary)',

        form: 'var(--bg-background-form)',
      },

      copy: {
        primary: 'var(--text-copy-primary)',
        secondary: 'var(--text-copy-hover)',
      },

      'border-color': {
        primary: 'var(--border-border-color-primary)',
      },

      transparent: 'transparent',

      black: '#000',
      white: '#fff',

      green: {
        base: 'var(--first-color)',
        100: 'hsl(var(--first-HS), 90%)',
        200: 'hsl(var(--first-HS), 80%)',
        300: 'hsl(var(--first-HS), 70%)',
        400: 'hsl(var(--first-HS), 60%)',
        500: 'hsl(var(--first-HS), 50%)',
        600: 'hsl(var(--first-HS), 40%)',
        700: 'hsl(var(--first-HS), 30%)',
        800: 'hsl(var(--first-HS), 20%)',
        900: 'hsl(var(--first-HS), 10%)',
      },

      gray: {
        100: '#f7fafc',
        200: '#edf2f7',
        300: '#e2e8f0',
        400: '#cbd5e0',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
      accent: {
        base: 'var(--accent-color)',
        100: 'hsl(var(--accent-HS), 90%)',
        200: 'hsl(var(--accent-HS), 80%)',
        300: 'hsl(var(--accent-HS), 70%)',
        400: 'hsl(var(--accent-HS), 60%)',
        500: 'hsl(var(--accent-HS), 50%)',
        600: 'hsl(var(--accent-HS), 40%)',
        700: 'hsl(var(--accent-HS), 30%)',
        800: 'hsl(var(--accent-HS), 20%)',
        900: 'hsl(var(--accent-HS), 10%)',
      }
    },
    fontFamily: {
      sans: [
        'Nunito Sans',
        'Roboto',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: ['Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: [
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace',
      ],
    },
  },
  variants: {
    // Some useful comment
  },
  plugins: [
    // Some useful comment
  ]
}
