/**
 * imagesResize
 * Resize all JPG images to three different sizes: 200, 500, and 630 pixels
 */
const imagesResize = {
  '*.{jpg,png,jpeg,gif,webp}': [{
    width: 100,
    suffix: '-100'
  }, {
    width: 100 * 2,
    suffix: '-100-2x'
  }],
  'slides/*.{jpg,png,jpeg,gif,webp}': [{
    width: 1280,
    suffix: ''
  }],
  'coros-orquestas/*.{jpg,png,jpeg,gif,webp}': [{
    width: 550,
    height: 480,
    crop: true,
    suffix: '-100'
  }, {
    width: 800,
    suffix: '-800'
  }],
  'escuela-musica/*.{jpg,png,jpeg,gif,webp}': [{
    width: 550,
    height: 450,
    crop: true,
    suffix: ''
  }],
  'profesores/*.{jpg,png,jpeg,gif,webp}': [{
    width: 400,
    height: 400,
    crop: true,
    suffix: ''
  }],
  'producciones/*.{jpg,png,jpeg,gif,webp}': [{
    width: 550,
    suffix: ''
  }],
};

module.exports = imagesResize;