export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros',
    icon: 'icon-us'
  },
  {
    title: 'Contactos',
    href: 'contactos',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Los profesores',
    href: 'los-profesores'
  },
  {
    title: 'Escuela de Musica',
    href: 'escuela-de-musica',
    submenu: [
      {
        title: 'Cuerdas',
        href: 'cuerdas'
      },
      {
        title: 'Canto',
        href: 'canto'
      },
      {
        title: 'Vientos',
        href: 'viento'
      }
    ]
  },
  {
    title: 'Producciones',
    href: 'producciones'
  },
  {
    title: 'Coros y orquestas',
    href: 'coros-y-orquestas'
  },
  {
    title: 'Solicita tu audición',
    href: 'solicitud-de-audicion',
    class: 'accent'
  }
]
