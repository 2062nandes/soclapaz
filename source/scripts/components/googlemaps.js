/*
  |--------------------------------------------------------------------------
  | Google Maps Javascript API
  |--------------------------------------------------------------------------
  | Add the api script of google maps with your ´YOUR_API_KEY´ before the script.js from project
  | Example Pug: ´script(src= "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY")´
  |
  */
export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data () {
    return {
      mapName: 'map',
      zoom: 2,
      center: {
        latitude: -16.4996904,
        longitude: -68.1263124
      },
      markerCoordinates: [
        {
          latitude: -16.4996904,
          longitude: -68.1263124,
          title: `<div id="content">
                      <h2 id="firstHeading" class="title text-white p-2 text-center">SOCIEDAD ORQUESTAL Y CORAL DE LA PAZ</h2>
                      <div id="bodyContent" class="px-2 pt-1 pb-3">
                        <p class="text-center"><b>Descripción:</b> Consolidar una organización de coros y orquestas<br>que sean referente de calidad nacional e internacional<br>caracterizados por la innovación, creación y formación artística.</p>
                        <p class="pt-2"><b>Dirección:</b> Av. Illimani 1914 Edif. Marecos · <b>Telefono:</b> 2-222715.</p>
                      </div>
                      <footer><a class="button--cta center" href="https://www.google.com/maps/place/Sociedad+orquestal+y+coral+de+La+Paz/@-16.4997672,-68.1284238,17z/data=!3m1!4b1!4m5!3m4!1s0x915f21a099711edb:0xda6ce9b5dc406bdd!8m2!3d-16.4997672!4d-68.1262298" target="_blank">Ver en google maps</a></footer>
                    </div>`
        }
      ]
    }
  },
  mounted () {
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: 17,
      center: new window.google.maps.LatLng(this.center.latitude, this.center.longitude)
    }
    const map = new window.google.maps.Map(element, options)
    /* MEDIA QUERIE ZOOM GOOGLE MAPS */
    const mediumBp = window.matchMedia('(min-width: 950px)')
    const changeSize = mql => {
      mql.matches
        ? map.setZoom(17)
        : map.setZoom(16)
    }
    mediumBp.addListener(changeSize)
    changeSize(mediumBp)
    this.markerCoordinates.forEach((coord) => {
      const position = new window.google.maps.LatLng(coord.latitude, coord.longitude)
      const marker = new window.google.maps.Marker({ position, map })
      const infowindow = new window.google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close()
      })
      window.google.maps.event.addListener(marker, 'click',
        (function (marker, coord, infowindow) {
          return function () {
            infowindow.setContent(coord.title)
            infowindow.open(map, marker)
            map.setZoom(17)
            map.setCenter(marker.getPosition())
          }
        })(marker, coord, infowindow))
    })
  }
}
