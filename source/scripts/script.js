import { menuToggle } from './modules/menu'
import { getdate } from './modules/date'
import { edTabs } from './modules/tabs'
import { goups } from './modules/goup'
// import { videoSize } from "./modules/video";
// import Data Vue
import { pathPage, pathMedia } from './data/routes'
import { menuinicio, mainmenu } from './data/menus'
import { contactform } from './data/contact-form'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import axios from 'axios/dist/axios.min'
import baguetteBox from 'baguettebox.js';
import { googleMap } from './components/googlemaps'
import { headroom } from 'vue-headroom'
import { Carousel, Slide } from 'vue-carousel';

// Vue Components
Vue.component('google-map', googleMap)

const rex = new Vue({
  el: '#rex',
  components: {
    headroom,
    Carousel,
    Slide
  },
  data: {
    menuActive: false,
    path_media: pathMedia,
    path_page: pathPage,
    menuinicio,
    mainmenu,
    formSubmitted: false,
    vue: contactform,
    classes: {
      pinned: 'headroom:pinned',
      unpinned: 'headroom:unpinned',
      unfixed: 'headroom:unfixed',
      top: 'headroom:top',
      notTop: 'headroom:not-top',
      bottom: 'headroom:bottom',
      notBottom: 'headroom:not-bottom',
      initial: 'headroom'
    }
  },
  mounted: function () {
    baguetteBox.run('.gallery', { bodyClass: 'baguetteBox:open' });
    window.edTabs = edTabs
    // tns(optionSlider)
    goups()
    menuToggle()
    getdate()
  },
  methods: {
    isFormValid: function () {
      return this.nombre !== ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      axios.post('../../mail.php', { vue: this.vue })
        .then((response) => {
          this.vue.envio = response.data
          this.clearForm()
        })
    },
    submitFormSolicitud: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      axios.post('../../solicitud.php', { vue: this.vue })
        .then((response) => {
          this.vue.envio = response.data
          this.clearForm()
        })
    },
    schoolEvent (type) {
      console.log(type)
    }
  }
})
Vue.use(rex)
