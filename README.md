# REDAXO mit GULP, TAILWINDCSS, DOCKER, Browserify, PostCSS, Pug, ES6 und GulTailKer

## Installation global requeriments
- Npm
- Docker
- Docker Compose
- Gulp

## Project setup
```
cp .env.example .env
```
```
npm install
```
```
docker-compose up -d
```

### Compiles assets and hot-reloads for development
```
gulp
```